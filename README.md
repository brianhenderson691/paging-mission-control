# Challenge - Program Notes

- Used Python39 with PIP39 for main program
- Used input.txt file with test data
- Used vi to write paging-mission-control.py python
  and input.txt file
- Used Visual Studio to edit this README file

Used the following libraries in program

- datetime (for comparing dates)
- timedelta (for comparing time difference)
- time (also for time difference)
- json (JSON dumps function for output to console
  in JSON format)

Used enough test data to trigger three of the four alarm messages.
One for each high temerature alarm on satellites 1000 and 1001 with
multiple alerts on 1001 to demonstrate continued issue with temperature.
Only issued one alert for the low voltage on battery issue on
satellite 1000 to allow for possible relation to high temperature issue.

This was my first time programming in Python, I chose it for a challenge to 
myself as it is common with DEVOPS community. I found the syntax to be easier 
than C++ and JAVA to some degree but challenging in small ways such as not 
declaring variables ahead of time and not ending loops, select statements etc.
as normally done. Also logic operaors are different such as and for && etc. 

There is additional logic I could have added but I wanted to keep it simple
and I had already hit close to 9.5 hours due to the following:

- Learning Python
- Learning how to manipulate datetime properly for the program's purpose


# ORIGINAL README from challenge below
---------------------------------------

# Paging Mission Control

> You are tasked with assisting satellite ground operations for an earth science mission that monitors magnetic 
field variations at the Earth's poles. A pair of satellites fly in tandem orbit such that at least one will have
 line of sight with a pole to take accurate readings. The satellite’s science instruments are sensitive to chang
es in temperature and must be monitored closely. Onboard thermostats take several temperature readings every min
ute to ensure that the precision magnetometers do not overheat. Battery systems voltage levels are also monitore
d to ensure that power is available to cooling coils. Design a monitoring and alert application that processes s
tatus telemetry from the satellites and generates alert messages in cases of certain limit violation scenarios.

## Requirements
Ingest status telemetry data and create alert messages for the following violation conditions:

- If for the same satellite there are three battery voltage readings that are under the red low limit within a f
ive minute interval.
- If for the same satellite there are three thermostat readings that exceed the red high limit within a five min
ute interval.

### Input Format
The program is to accept a file as input. The file is an ASCII text file containing pipe delimited records.

The ingest of status telemetry data has the format:

```
<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<
component>
```

You may assume that the input files are correctly formatted. Error handling for invalid input files may be ommit
ted.

### Output Format
The output will specify alert messages.  The alert messages should be valid JSON with the following properties:

```javascript
{
    "satelliteId": 1234,
    "severity": "severity",
    "component": "component",
    "timestamp": "timestamp"
}
```

The program will output to screen or console (and not to a file). 

## Sample Data
The following may be used as sample input and output datasets.

### Input

```
20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT
```

### Ouput

```javascript
[
    {
        "satelliteId": 1000,
        "severity": "RED HIGH",
        "component": "TSTAT",
        "timestamp": "2018-01-01T23:01:38.001Z"
    },
    {
        "satelliteId": 1000,
        "severity": "RED LOW",
        "component": "BATT",
        "timestamp": "2018-01-01T23:01:09.521Z"
    }
]
```
