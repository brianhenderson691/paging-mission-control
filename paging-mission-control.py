######################
# Import libraries
######################

from datetime import datetime as dt, timedelta
import json
import time

####################
#Set datetime format
####################

fmt = "%Y%m%d %H:%M:%S.%f"

################################################
# Import first time stamp from text file for
# first time stamp variable
################################################


with open('input.txt') as file:
  
  firsttimestampingest = file.readline(21)
    
  firsttimestamp = dt.strptime(firsttimestampingest, fmt) #'%Y %m %d %H %M %S .%f')

file.close()


################################################
# Open text file to import data
################################################

with open('input.txt', 'r') as f:

##################################
# Set initial count for limits
##################################

  sat1000_red_low_limit_count = 0
  sat1001_red_low_limit_count = 0
  sat1000_red_high_limit_count = 0
  sat1001_red_high_limit_count = 0


################################################
# Use loop to ingest all data from file 
################################################


  for line in f:
     line = line.strip("\n")
     timestamp, satelliteId, red_high_limit, yellow_high_limit, yellow_low_limit, red_low_limit, raw_value, comp
onent = line.split('|')
     timestamp = dt.strptime(timestamp, fmt) #'%Y %m %d %H %M %S .%f')


################################################
##Converting data types as needed
################################################ 
 
     satelliteId = int(satelliteId)
     red_high_limit = int(red_high_limit)
     red_low_limit = int(red_low_limit)

#####################################################################
## First satellite check (1000) for red low limit Battery voltage
#####################################################################


     if (satelliteId == 1000 and component == "BATT" and red_low_limit < 10):
       sat1000_red_low_limit_count = sat1000_red_low_limit_count + 1
       
     if (satelliteId == 1000 and sat1000_red_low_limit_count >= 3):
       timestamp1000rl2 = timestamp
       timestamp1000rl1 = firsttimestamp
       difference = timestamp1000rl2 - timestamp1000rl1
       if (difference > timedelta(minutes=5)):
         low_1000 = {
                 "satelliteId": satelliteId,
                 "severity": "RED LOW",
                 "component": "BATT",
                 "timestamp": str(timestamp1000rl2)
         }  
         a = json.dumps(low_1000)
         print(a)


#####################################################################
## Second satellite check (1001) for red low limit Battery voltage
#####################################################################


     if (satelliteId == 1001 and component == "BATT" and red_low_limit < 10):
       sat1001_red_low_limit_count = sat1001_red_low_limit_count + 1
    
     if (satelliteId == 1001 and sat1001_red_low_limit_count >= 3):
       timestamp1001rl2 = timestamp
       timestamp1001rl1 = firsttimestep
       difference = timestamp1001rl2 - timestamp1001rl1
       if (difference > timedelta(minutes=5)):
         low_1001 = {
                    "satelliteId": satelliteId,
                    "severity": "RED LOW",
                    "component": "BATT",
                    "timestamp": str(timestamp1001rl2)
         }
         b = json.dumps(low_1001)
         print(b)


#####################################################################
## First satellite check (1000) for red high limit temperature
#####################################################################


     if (satelliteId == 1000 and component == "TSTAT" and red_high_limit > 90):
       sat1000_red_high_limit_count = sat1000_red_high_limit_count + 1
       
     if (satelliteId == 1000 and sat1000_red_high_limit_count >= 3):
       timestamp1000rh2 = timestamp
       timestamp1000rh1 = firsttimestamp
       difference = timestamp1000rh2 - timestamp1000rh1
       if (difference > timedelta(minutes=5)):
         high_1000 = {
                     "satelliteId": satelliteId,
                     "severity": "RED HIGH",
                     "component": "TSTAT",
                     "timestamp": str(timestamp1000rh2)
         }
         c = json.dumps(high_1000)
         print(c)


#####################################################################
## Second satellite check (1001) for red high limit temperature
#####################################################################


     if (satelliteId == 1001 and component == "TSTAT" and red_high_limit > 90):
       sat1001_red_high_limit_count = sat1001_red_high_limit_count + 1
    
     if (satelliteId == 1001 and sat1001_red_high_limit_count >= 3):
       timestamp1001rh2 = timestamp
       timestamp1001rh1 = firsttimestamp
       difference = timestamp1001rh2 - timestamp1001rh1
       if (difference > timedelta(minutes=5)):
         high_1001 = {
                     "satelliteId": satelliteId,
                     "severity": "RED HIGH",
                     "component": "TSTAT",
                     "timestamp": str(timestamp1001rh2)
         }
         d = json.dumps(high_1001)
         print(d)


#####################################################################
## End of program
#####################################################################